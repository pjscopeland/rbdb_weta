Run options: include {:focus=>true}

All examples were filtered out; ignoring {:focus=>true}

./bin/load
  with -h
    should print usage instructions
  without an input file
    should report failure
    should say what went wrong
  field issues
    with a file missing a field
      should report failure
      should say what went wrong
      with an output file
        should say what went wrong on stdout
        should not write a file
    with a file with an extra field
      should report failure
      should say what went wrong
      with an output file
        should say what went wrong on stdout
        should not write a file
    with a file with a misspelled field
      should report failure
      should say what went wrong
      with an output file
        should say what went wrong on stdout
        should not write a file
    with a file with a duplicated field
      should report failure
      should say what went wrong
      with an output file
        should say what went wrong on stdout
        should not write a file
    with a file with reordered fields
      should report success
      should output a PSV
      with an output file
        should write the PSV to a file
  validation issues
    with a long PROJECT
      should report failure
      should say what went wrong
      with an output file
        should say what went wrong on stdout
        should not write a file
    with a long SHOT
      should report failure
      should say what went wrong
      with an output file
        should say what went wrong on stdout
        should not write a file
    with a bad VERSION
      should report failure
      should say what went wrong
      with an output file
        should say what went wrong on stdout
        should not write a file
    with a high VERSION
      should report failure
      should say what went wrong
      with an output file
        should say what went wrong on stdout
        should not write a file
    with a low VERSION
      should report failure
      should say what went wrong
      with an output file
        should say what went wrong on stdout
        should not write a file
    with a bad INTERNAL_BID
      should report failure
      should say what went wrong
      with an output file
        should say what went wrong on stdout
        should not write a file
    with a high INTERNAL_BID
      should report failure
      should say what went wrong
      with an output file
        should say what went wrong on stdout
        should not write a file
    with a low INTERNAL_BID
      should report failure
      should say what went wrong
      with an output file
        should say what went wrong on stdout
        should not write a file
    with a long STATUS
      should report failure
      should say what went wrong
      with an output file
        should say what went wrong on stdout
        should not write a file
    with an invalid FINISH_DATE
      should report failure
      should say what went wrong
      with an output file
        should say what went wrong on stdout
        should not write a file
    with an invalid FINISH_DATE
      should report failure
      should say what went wrong
      with an output file
        should say what went wrong on stdout
        should not write a file
    with an invalid FINISH_DATE
      should report failure
      should say what went wrong
      with an output file
        should say what went wrong on stdout
        should not write a file
    with an invalid FINISH_DATE
      should report failure
      should say what went wrong
      with an output file
        should say what went wrong on stdout
        should not write a file
    with an invalid CREATED_DATE
      should report failure
      should say what went wrong
    with an invalid CREATED_DATE
      should report failure
      should say what went wrong
      with an output file
        should say what went wrong on stdout
        should not write a file
    with an invalid CREATED_DATE
      should report failure
      should say what went wrong
      with an output file
        should say what went wrong on stdout
        should not write a file
    with an invalid CREATED_DATE
      should report failure
      should say what went wrong
  with a good input file
    should report success
    should output a PSV
    should not contain the duplicated line
    with an output file
      should write the PSV to a file

./bin/query
  with -h
    should print usage instructions
  with a filter
    with = operator
      and string field
        should report success
        should return the fields where the values match exactly
      and integer field
        should report success
        should return the fields where the values match exactly
      and float field
        should report success
        should return the fields where the values match exactly
      and date field
        should report success
        should return the fields where the values match exactly
    with \< operator
      and string field
        should report success
        should return the fields where the values are less
      and integer field
        should report success
        should return the fields where the values are less
      and float field
        should report success
        should return the fields where the values are less
      and date field
        should report success
        should return the fields where the values are less
    with \> operator
      and string field
        should report success
        should return the fields where the values are greater
      and integer field
        should report success
        should return the fields where the values are greater
      and float field
        should report success
        should return the fields where the values are greater
      and date field
        should report success
        should return the fields where the values are greater
    with \<= operator
      and string field
        should report success
        should return the fields where the values are less or equal
      and integer field
        should report success
        should return the fields where the values are less or equal
      and float field
        should report success
        should return the fields where the values are less or equal
      and date field
        should report success
        should return the fields where the values are less or equal
    with \>= operator
      and string field
        should report success
        should return the fields where the values are greater or equal
      and integer field
        should report success
        should return the fields where the values are greater or equal
      and float field
        should report success
        should return the fields where the values are greater or equal
      and date field
        should report success
        should return the fields where the values are greater or equal
    with != operator
      and string field
        should report success
        should return the fields where the values do not match
      and integer field
        should report success
        should return the fields where the values do not match
      and float field
        should report success
        should return the fields where the values do not match
      and date field
        should report success
        should return the fields where the values do not match
  with multiple filters
    joined by AND
      should report success
      should return the fields where all the filters match
    joined by OR
      should report success
      should return the fields where all the filters match
    both AND and OR
      should report success
      should return the fields where all the filters match
    both AND and bracketed OR
      should report success
      should return the fields where all the filters match
    Bobby Tables
      should report failure
      should say what went wrong
      should leave the file intact
  with an order
    on a string field
      should report success
      should return the records in the given order
    on an integer field
      should report success
      should return the records in the given order
    on a float field
      should report success
      should return the records in the given order
    on a date field
      should report success
      should return the records in the given order
    on multiple fields
      should report success
      should return the records in the given order
  with a field selected
    should report success
    should return the selected field
  with multiple fields selected
    should report success
    should return the selected fields in the order given
  with grouping and good aggregates
    should report success
    should return the selected fields aggregated correctly
  with grouping and bad aggregates
    should report failure
    should say what went wrong
  with grouping and unaggregated fields
    should report failure
    should say what went wrong
  with good aggregates but no grouping
    should report success
    should aggregate all records
  with aggregates and unaggregated fields but no grouping
    should report failure
    should say what went wrong
  with bad aggregates but no grouping
    should report failure
    should say what went wrong

Finished in 37.2 seconds (files took 0.6027 seconds to load)
176 examples, 0 failures

