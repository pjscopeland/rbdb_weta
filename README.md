# rbdb_weta

A database written in Ruby (hence RbDB) for a technical test for Weta Digital.

## Technologies

I use RVM as a Ruby Version Manager. This project was initially written in Ruby 2.6.3 (currently the version listed in the .ruby-version file).

### Development Gems

I have a set of gems that I use for development, but they are not required for running the project. The main ones are:

- Test-Driven Development was made possible by [RSpec](https://rspec.info/).
- Code style consistency was enforced by [Rubocop](https://rubocop.readthedocs.io/).
- Consoles were powered by [Pry](https://github.com/pry/pry).

## Database

For querying purposes, the database is expected to be stored in a flat PSV file called `shot_versions.psv`, but you can specify a different location in each script. A PSV (pipe separated value) file is very similar to a CSV, but uses `|` instead of `,` to delimit fields. I used PSV for storage as well as input to simplify loading processes.

(Further development of RbDB would require one file per table, with configurable names, but that could be implemented at such time as it becomes necessary. I've simply prototyped a single table for the moment.)

## Usage

### Loader

~~~bash
./bin/load INPUT_FILE [OUTPUT_FILE]
~~~

Loads a pipe-separated-value (PSV) file, validates each record, removes duplicate values (by PROJECT, SHOT and VERSION - keeping the last one entered), and outputs the results to another PSV.

If you omit `OUTPUT_FILE`, the results will be output to stdout.

If you include an `OUTPUT_FILE` that already exists, it will treat that file as the datastore and treat the `INPUT_FILE` as an update to it. If you want to completely overwrite the datastore, you can use `> OUTPUT_FILE` instead.

### Querier

~~~bash
Usage: ./bin/query OPTIONS [INPUT_FILE=shot_versions.psv]
~~~

Loads a pipe-separated-value (PSV) file (delimited by '|') and queries
values in it.

`INPUT_FILE` defaults to './shot_versions.psv'.

OPTIONS:
- `-s`, `--select FIELDS`
  Select the given comma-delimited fields. You can aggregate fields by using the format `FIELD:FUNCTION`, where `FUNCTION` is any one of `{min|max|sum|count|collect}`. If aggregating, fields in the `--select` list must either have an aggregate function attached or also be in the `--group` list. (Do not include spaces in the field list.)

- `-o`, `--order FIELDS`
  Order by the given comma-delimited fields. (Do not include spaces in the field list.)

- `-f`, `--filter FIELDS`
  Filter by the given fields. FIELDS should be in the format `<FIELD><OPERATOR><VALUE>[ {AND|OR} ...]`, where `OPERATOR` can be any one of `{=|<|>|<=|>=|!=}`. `<` and `>` must be preceded with `\` unless surrounded by quote marks. `VALUE` must be surrounded by quote marks if it contains a space.

- `-g`, `--group FIELDS`
  Group by the given fields. If aggregating, fields in the `--select` list must either have an aggregate function attached or also be in the `--group` list. (Do not include spaces in the field list.)
