require_relative 'hash_mixins'

class ShotVersion
  EXPECTED_FIELDS = %w[PROJECT SHOT VERSION STATUS FINISH_DATE INTERNAL_BID CREATED_DATE].freeze
  UNIQUE_FIELDS = %w[PROJECT SHOT VERSION].freeze

  attr_reader :errors

  def initialize
    @data = []
    @errors = []
  end

  def self.parse_psv(path)
    content = File.read(path)
    if content =~ /\S/ # normally I'd use #blank? but apparently that's ActiveSupport...
      array_of_arrays = content.split("\n").map { |l| l.split('|') }

      actual_fields = array_of_arrays[0]

      # The fields can be out of order, we can cope with that
      unless actual_fields.sort == EXPECTED_FIELDS.sort
        # Array#% is similar to Array#- but doesn't remove duplicates. See array_mixins.rb
        (missing = EXPECTED_FIELDS % actual_fields)
        (extra = actual_fields % EXPECTED_FIELDS)

        puts [
          "Incorrect fields in #{path}.",
          (missing.any? ? "Missing: #{missing.map { |s| "'#{s}'" }.join(', ')}" : nil),
          (extra.any? ? "Extra: #{extra.map { |s| "'#{s}'" }.join(', ')}" : nil)
        ].compact.join(' ')

        exit(1)
      end

      # ... but we're going to use the order we expect when we output the fields.
      array_of_arrays[1..-1].map do |a|
        EXPECTED_FIELDS.map { |k| [k, a[actual_fields.index(k)]] }.to_h
      end
    else
      []
    end
  end

  def add(more_data)
    # I could use += here but that would reassign the combined array to a new
    # object.
    @data.concat more_data
  end

  def valid?
    @errors = []
    @data.each_with_index do |hash, i|
      @errors << "PROJECT is too long (max 64 characters) on line #{i + 1}" if hash['PROJECT'].length > 64

      @errors << "SHOT is too long (max 64 characters) on line #{i + 1}" if hash['SHOT'].length > 64

      # Integer() is stricter than #to_i - raising errors for blanks rather than returning 0, for example.
      version_valid = (0..65_535).include?(Integer(hash['VERSION'])) rescue false
      @errors << "VERSION is invalid (must be an integer from 0 to 65535) on line #{i + 1}" unless version_valid

      @errors << "STATUS is too long (max 32 characters) on line #{i + 1}" if hash['STATUS'].length > 32

      finish = hash['FINISH_DATE']
      finish_valid = finish =~ /\A\d{4}-(?:0[1-9]|1[0-2])-(?:0[1-9]|[1-2]\d|3[01])\z/
      @errors << "FINISH_DATE is invalid (must be a date in YYYY-MM-DD format) on line #{i + 1}" unless finish_valid

      bid_valid = (0..65_535).include?(Float(hash['INTERNAL_BID'])) rescue false
      @errors << "INTERNAL_BID is invalid (must be a number between 0 and 65535) on line #{i + 1}" unless bid_valid

      created = hash['CREATED_DATE']
      created_valid = created =~ /\A\d{4}-(?:0[1-9]|1[0-2])-(?:0[1-9]|[1-2]\d|3[01]) ([01]\d|2[0-3]):([0-5]\d)\z/
      unless created_valid
        @errors << "CREATED_DATE is invalid (must be a time stamp in YYYY-MM-DD hh:mm format) on line #{i + 1}"
      end
    end

    @errors.empty?
  end

  # We're going to exploit Ruby's inherent Hash merging tactic (new-replaces-old)
  # to ensure uniqueness across the fields.
  def uniq
    hashes_of_hashes = @data.map do |h|
      {h.slice(*UNIQUE_FIELDS) => h.except(*UNIQUE_FIELDS)}
    end
    hashes_of_hashes.inject(:merge).map { |k, v| k.merge(v) }
  end

  def uniq!
    @data.replace uniq
  end

  def to_s
    array_of_arrays = if @data.empty?
      [EXPECTED_FIELDS]
    else
      [@data[0].keys] + @data.map(&:values)
    end
    # Rubocop wants to change $/ to $INPUT_RECORD_SEPARATOR, but for some reason
    # it's not defined in RSpec.
    array_of_arrays.map { |a| a.join('|') }.join($/) # rubocop:disable Style/SpecialGlobalVars
  end

  # QUERYING METHODS

  def filter(filters = nil)
    return @data unless filters

    # We're going to be #evalling the filter results later. This is open to
    # injection attacks, so we're going to be enforcing very strict filter
    # formats.
    rgx_core = "(#{EXPECTED_FIELDS.join('|')})(=|<=?|>=?|!=)(\"[\\w\\-:.' ]+\"|'[\\w\\-:.\" ]+'|[\\w\\-:.'\"]+)"
    filter_token_rgx = /#{rgx_core}/
    filter_collection_rgx = /\A(?:\(*#{rgx_core}\)*(?: +(AND|OR) +|\z))+\z/
    unless filters =~ filter_collection_rgx && filters.count('(') == filters.count(')')
      puts "Syntax error in #{filters}"
      exit 1
    end

    @data.select do |record|
      eval_string = filters.gsub(filter_token_rgx) do |_match|
        field = Regexp.last_match(1)
        operator = Regexp.last_match(2)
        filter_value = Regexp.last_match(3).gsub(/^['"]|['"]$/, '')
        # Parse the field, both in the data and in the filter
        case field
        when 'SHOT', 'VERSION'
          record_value = record[field].to_i
          filter_value = Integer(filter_value)
        when 'INTERNAL_BID'
          record_value = record[field].to_f
          filter_value = Float(filter_value)
        else
          # date values are in ISO8601 and are thus comparable lexographically
          record_value = record[field]
        end

        # Compare the values
        case operator
        when '=' then record_value == filter_value
        when '<' then record_value < filter_value
        when '>' then record_value > filter_value
        when '<=' then record_value <= filter_value
        when '>=' then record_value >= filter_value
        when '!=' then record_value != filter_value
        end
      end.downcase

      eval(eval_string) # rubocop:disable Security/Eval - we've mitigated the risks by checking the format above
    end
  end

  def filter!(filters = nil)
    @data.replace filter(filters)
  end

  def group(groups, aggregates)
    array_of_hashes = @data.map do |r|
      {
        r.slice(*groups) =>
        r.except(*groups).map do |k, v|
          [
            k,
            case aggregates.to_h[k]
            when 'collect' then "[#{v}]"
            when 'count' then 1
            else v
            end
          ]
        end.to_h
      }
    end
    hashes_of_hashes = array_of_hashes.inject do |output, row|
      output.merge(row) do |_key, old_hash, new_hash|
        old_hash.merge(new_hash) do |field, old_value, new_value|
          case aggregates.to_h[field]
          when 'min' then [old_value, new_value].min
          when 'max' then [old_value, new_value].max
          when 'sum'
            case field
            when 'SHOT', 'VERSION'
              [old_value.to_i, new_value.to_i].sum.to_s
            when 'INTERNAL_BID'
              format('%.2f', [old_value.to_f, new_value.to_f].sum)
            else
              puts "Can't sum the #{field} field"
              exit 1
            end
          when 'count'then old_value + 1
          when 'collect' then "[#{old_value[/^\[(.*)\]$/, 1]},#{new_value[/^\[(.*)\]$/, 1]}]"
          end
        end
      end
    end
    hashes_of_hashes.map { |k, v| k.merge(v) }
  end

  def group!(groups, aggregates)
    @data.replace group(groups, aggregates)
  end

  def order(orders = nil)
    return @data unless orders

    @data.sort_by do |record|
      orders.map do |field|
        case field
        when 'SHOT', 'VERSION' then record[field].to_i
        when 'INTERNAL_BID' then record[field].to_f
        else record[field]
        end
      end
    end
  end

  def order!(orders)
    @data.replace order(orders)
  end

  def select_fields(selects = nil)
    return @data unless selects

    @data.map do |record|
      selects.map { |field| [field, record[field]] }.to_h
    end
  end

  def select_fields!(selects = nil)
    @data.replace select_fields(selects)
  end
end
