class Hash
  def except(*key_args)
    reject { |k, _v| key_args.include? k }
  end
end
