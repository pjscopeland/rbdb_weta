class Array
  # Array subtraction - but where an element is duplicated in the receiver, it
  # only subtracts the number of instances in the argument.
  #
  # [1, 1, 2, 3, 5] - [1] => [2, 3, 5]
  # [1, 1, 2, 3, 5] % [1] => [1, 2, 3, 5]
  def subtract(other)
    output = dup
    other.each do |element|
      if (i = output.index(element))
        output.delete_at(i)
      end
    end
    output
  end
  alias % subtract
end
