require '~/lib/spec_helper'

describe './bin/load' do
  let(:args) { [input_file, output_file].join(' ') }
  let(:cmd) { "./bin/load #{args}" }
  # These must exist, but needn't be initialised
  let(:input_file) {}
  let(:output_file) {}

  before { FileUtils.rm(output_file) if output_file && File.exist?(output_file) }

  subject(:ran_successfully?) { system("#{cmd} >/dev/null 2>&1") }
  subject(:stdout_err) { `#{cmd}`.strip }
  subject(:file_output) { File.read(output_file).strip }

  context 'with -h' do
    let(:args) { '-h' }

    it 'should print usage instructions' do
      expect(stdout_err).to match %r{^Usage: \./bin/load INPUT_FILE \[OUTPUT_FILE\]$}
    end
  end

  context 'without an input file' do
    it 'should report failure' do
      expect(ran_successfully?).to be false
    end

    it 'should say what went wrong' do
      expect(stdout_err).to eq 'No input file given'
    end

    # An output file is the second one specified, so having "only an output
    # file" makes no sense
  end

  describe 'field issues' do
    context 'with a file missing a field' do
      let(:input_file) { 'spec/files/field_missing.psv' }
      let(:error_message) { "Incorrect fields in spec/files/field_missing.psv. Missing: 'INTERNAL_BID'" }

      it 'should report failure' do
        expect(ran_successfully?).to be false
      end

      it 'should say what went wrong' do
        expect(stdout_err).to eq error_message
      end

      context 'with an output file' do
        let(:output_file) { 'spec/files/output.psv' }

        it 'should say what went wrong on stdout' do
          expect(stdout_err).to eq error_message
        end

        it 'should not write a file' do
          expect { stdout_err }.not_to change { File.exist?(output_file) }.from(false)
        end
      end
    end

    context 'with a file with an extra field' do
      let(:input_file) { 'spec/files/field_extra.psv' }
      let(:error_message) { "Incorrect fields in spec/files/field_extra.psv. Extra: 'EXTRA'" }

      it 'should report failure' do
        expect(ran_successfully?).to be false
      end

      it 'should say what went wrong' do
        expect(stdout_err).to eq error_message
      end

      context 'with an output file' do
        let(:output_file) { 'spec/files/output.psv' }

        it 'should say what went wrong on stdout' do
          expect(stdout_err).to eq error_message
        end

        it 'should not write a file' do
          expect { stdout_err }.not_to change { File.exist?(output_file) }.from(false)
        end
      end
    end

    context 'with a file with a misspelled field' do
      let(:input_file) { 'spec/files/field_wrong.psv' }
      let(:error_message) do
        "Incorrect fields in spec/files/field_wrong.psv. Missing: 'INTERNAL_BID' Extra: 'EXTERNAL_BID'"
      end

      it 'should report failure' do
        expect(ran_successfully?).to be false
      end

      it 'should say what went wrong' do
        expect(stdout_err).to eq error_message
      end

      context 'with an output file' do
        let(:output_file) { 'spec/files/output.psv' }

        it 'should say what went wrong on stdout' do
          expect(stdout_err).to eq(error_message)
        end

        it 'should not write a file' do
          expect { stdout_err }.not_to change { File.exist?(output_file) }.from(false)
        end
      end
    end

    context 'with a file with a duplicated field' do
      let(:input_file) { 'spec/files/field_duplicated.psv' }
      let(:error_message) { "Incorrect fields in spec/files/field_duplicated.psv. Extra: 'SHOT'" }

      it 'should report failure' do
        expect(ran_successfully?).to be false
      end

      it 'should say what went wrong' do
        expect(stdout_err).to eq error_message
      end

      context 'with an output file' do
        let(:output_file) { 'spec/files/output.psv' }

        it 'should say what went wrong on stdout' do
          expect(stdout_err).to eq error_message
        end

        it 'should not write a file' do
          expect { stdout_err }.not_to change { File.exist?(output_file) }.from(false)
        end
      end
    end

    context 'with a file with reordered fields' do
      let(:input_file) { 'spec/files/field_reordered.psv' }

      it 'should report success' do
        expect(ran_successfully?).to be true
      end

      it 'should output a PSV' do
        expect(stdout_err).to eq <<~PSV.strip
          PROJECT|SHOT|VERSION|STATUS|FINISH_DATE|INTERNAL_BID|CREATED_DATE
          the hobbit|01|64|scheduled|2010-05-15|45.00|2010-04-01 13:35
          lotr|03|16|finished|2001-05-15|15.00|2001-04-01 06:47
          king kong|42|128|not required|2006-07-22|30.00|2006-10-15 09:14
          the hobbit|40|32|finished|2010-05-15|22.80|2010-03-22 01:10
        PSV
      end

      context 'with an output file' do
        let(:output_file) { 'spec/files/output.psv' }

        it 'should write the PSV to a file' do
          expect do
            expect(stdout_err).to eq "Results written to #{output_file}"
          end.to change { File.exist?(output_file) }.from(false).to(true)

          expect(file_output).to eq <<~PSV.strip
            PROJECT|SHOT|VERSION|STATUS|FINISH_DATE|INTERNAL_BID|CREATED_DATE
            the hobbit|01|64|scheduled|2010-05-15|45.00|2010-04-01 13:35
            lotr|03|16|finished|2001-05-15|15.00|2001-04-01 06:47
            king kong|42|128|not required|2006-07-22|30.00|2006-10-15 09:14
            the hobbit|40|32|finished|2010-05-15|22.80|2010-03-22 01:10
          PSV
        end
      end
    end
  end

  describe 'validation issues' do
    context 'with a long PROJECT' do
      let(:input_file) { 'spec/files/invalid_project_long.psv' }
      let(:error_message) do
        <<~STR.strip
          Validation error in spec/files/invalid_project_long.psv:
          PROJECT is too long (max 64 characters) on line 1
        STR
      end

      it 'should report failure' do
        expect(ran_successfully?).to be false
      end

      it 'should say what went wrong' do
        expect(stdout_err).to eq error_message
      end

      context 'with an output file' do
        let(:output_file) { 'spec/files/output.psv' }

        it 'should say what went wrong on stdout' do
          expect(stdout_err).to eq error_message
        end

        it 'should not write a file' do
          expect { stdout_err }.not_to change { File.exist?(output_file) }.from(false)
        end
      end
    end

    context 'with a long SHOT' do
      let(:input_file) { 'spec/files/invalid_shot_long.psv' }
      let(:error_message) do
        <<~STR.strip
          Validation error in spec/files/invalid_shot_long.psv:
          SHOT is too long (max 64 characters) on line 2
        STR
      end

      it 'should report failure' do
        expect(ran_successfully?).to be false
      end

      it 'should say what went wrong' do
        expect(stdout_err).to eq error_message
      end

      context 'with an output file' do
        let(:output_file) { 'spec/files/output.psv' }

        it 'should say what went wrong on stdout' do
          expect(stdout_err).to eq error_message
        end

        it 'should not write a file' do
          expect { stdout_err }.not_to change { File.exist?(output_file) }.from(false)
        end
      end
    end

    context 'with a bad VERSION' do
      let(:input_file) { 'spec/files/invalid_version_bad.psv' }
      let(:error_message) do
        <<~STR.strip
          Validation error in spec/files/invalid_version_bad.psv:
          VERSION is invalid (must be an integer from 0 to 65535) on line 1
        STR
      end

      it 'should report failure' do
        expect(ran_successfully?).to be false
      end

      it 'should say what went wrong' do
        expect(stdout_err).to eq error_message
      end

      context 'with an output file' do
        let(:output_file) { 'spec/files/output.psv' }

        it 'should say what went wrong on stdout' do
          expect(stdout_err).to eq error_message
        end

        it 'should not write a file' do
          expect { stdout_err }.not_to change { File.exist?(output_file) }.from(false)
        end
      end
    end

    context 'with a high VERSION' do
      let(:input_file) { 'spec/files/invalid_version_high.psv' }
      let(:error_message) do
        <<~STR.strip
          Validation error in spec/files/invalid_version_high.psv:
          VERSION is invalid (must be an integer from 0 to 65535) on line 1
        STR
      end

      it 'should report failure' do
        expect(ran_successfully?).to be false
      end

      it 'should say what went wrong' do
        expect(stdout_err).to eq error_message
      end

      context 'with an output file' do
        let(:output_file) { 'spec/files/output.psv' }

        it 'should say what went wrong on stdout' do
          expect(stdout_err).to eq error_message
        end

        it 'should not write a file' do
          expect { stdout_err }.not_to change { File.exist?(output_file) }.from(false)
        end
      end
    end

    context 'with a low VERSION' do
      let(:input_file) { 'spec/files/invalid_version_low.psv' }
      let(:error_message) do
        <<~STR.strip
          Validation error in spec/files/invalid_version_low.psv:
          VERSION is invalid (must be an integer from 0 to 65535) on line 1
        STR
      end

      it 'should report failure' do
        expect(ran_successfully?).to be false
      end

      it 'should say what went wrong' do
        expect(stdout_err).to eq error_message
      end

      context 'with an output file' do
        let(:output_file) { 'spec/files/output.psv' }

        it 'should say what went wrong on stdout' do
          expect(stdout_err).to eq error_message
        end

        it 'should not write a file' do
          expect { stdout_err }.not_to change { File.exist?(output_file) }.from(false)
        end
      end
    end

    context 'with a bad INTERNAL_BID' do
      let(:input_file) { 'spec/files/invalid_bid_bad.psv' }
      let(:error_message) do
        <<~STR.strip
          Validation error in spec/files/invalid_bid_bad.psv:
          INTERNAL_BID is invalid (must be a number between 0 and 65535) on line 3
        STR
      end

      it 'should report failure' do
        expect(ran_successfully?).to be false
      end

      it 'should say what went wrong' do
        expect(stdout_err).to eq error_message
      end

      context 'with an output file' do
        let(:output_file) { 'spec/files/output.psv' }

        it 'should say what went wrong on stdout' do
          expect(stdout_err).to eq error_message
        end

        it 'should not write a file' do
          expect { stdout_err }.not_to change { File.exist?(output_file) }.from(false)
        end
      end
    end

    context 'with a high INTERNAL_BID' do
      let(:input_file) { 'spec/files/invalid_bid_high.psv' }
      let(:error_message) do
        <<~STR.strip
          Validation error in spec/files/invalid_bid_high.psv:
          INTERNAL_BID is invalid (must be a number between 0 and 65535) on line 3
        STR
      end

      it 'should report failure' do
        expect(ran_successfully?).to be false
      end

      it 'should say what went wrong' do
        expect(stdout_err).to eq error_message
      end

      context 'with an output file' do
        let(:output_file) { 'spec/files/output.psv' }

        it 'should say what went wrong on stdout' do
          expect(stdout_err).to eq error_message
        end

        it 'should not write a file' do
          expect { stdout_err }.not_to change { File.exist?(output_file) }.from(false)
        end
      end
    end

    context 'with a low INTERNAL_BID' do
      let(:input_file) { 'spec/files/invalid_bid_low.psv' }
      let(:error_message) do
        <<~STR.strip
          Validation error in spec/files/invalid_bid_low.psv:
          INTERNAL_BID is invalid (must be a number between 0 and 65535) on line 3
        STR
      end

      it 'should report failure' do
        expect(ran_successfully?).to be false
      end

      it 'should say what went wrong' do
        expect(stdout_err).to eq error_message
      end

      context 'with an output file' do
        let(:output_file) { 'spec/files/output.psv' }

        it 'should say what went wrong on stdout' do
          expect(stdout_err).to eq error_message
        end

        it 'should not write a file' do
          expect { stdout_err }.not_to change { File.exist?(output_file) }.from(false)
        end
      end
    end

    context 'with a long STATUS' do
      let(:input_file) { 'spec/files/invalid_status_long.psv' }
      let(:error_message) do
        <<~STR.strip
          Validation error in spec/files/invalid_status_long.psv:
          STATUS is too long (max 32 characters) on line 4
        STR
      end

      it 'should report failure' do
        expect(ran_successfully?).to be false
      end

      it 'should say what went wrong' do
        expect(stdout_err).to eq error_message
      end

      context 'with an output file' do
        let(:output_file) { 'spec/files/output.psv' }

        it 'should say what went wrong on stdout' do
          expect(stdout_err).to eq error_message
        end

        it 'should not write a file' do
          expect { stdout_err }.not_to change { File.exist?(output_file) }.from(false)
        end
      end
    end

    context 'with an invalid FINISH_DATE' do
      let(:input_file) { 'spec/files/invalid_finish_day_high.psv' }
      let(:error_message) do
        <<~STR.strip
          Validation error in spec/files/invalid_finish_day_high.psv:
          FINISH_DATE is invalid (must be a date in YYYY-MM-DD format) on line 5
        STR
      end

      it 'should report failure' do
        expect(ran_successfully?).to be false
      end

      it 'should say what went wrong' do
        expect(stdout_err).to eq error_message
      end

      context 'with an output file' do
        let(:output_file) { 'spec/files/output.psv' }

        it 'should say what went wrong on stdout' do
          expect(stdout_err).to eq error_message
        end

        it 'should not write a file' do
          expect { stdout_err }.not_to change { File.exist?(output_file) }.from(false)
        end
      end
    end

    context 'with an invalid FINISH_DATE' do
      let(:input_file) { 'spec/files/invalid_finish_day_low.psv' }
      let(:error_message) do
        <<~STR.strip
          Validation error in spec/files/invalid_finish_day_low.psv:
          FINISH_DATE is invalid (must be a date in YYYY-MM-DD format) on line 5
        STR
      end

      it 'should report failure' do
        expect(ran_successfully?).to be false
      end

      it 'should say what went wrong' do
        expect(stdout_err).to eq error_message
      end

      context 'with an output file' do
        let(:output_file) { 'spec/files/output.psv' }

        it 'should say what went wrong on stdout' do
          expect(stdout_err).to eq error_message
        end

        it 'should not write a file' do
          expect { stdout_err }.not_to change { File.exist?(output_file) }.from(false)
        end
      end
    end

    context 'with an invalid FINISH_DATE' do
      let(:input_file) { 'spec/files/invalid_finish_month_high.psv' }
      let(:error_message) do
        <<~STR.strip
          Validation error in spec/files/invalid_finish_month_high.psv:
          FINISH_DATE is invalid (must be a date in YYYY-MM-DD format) on line 5
        STR
      end

      it 'should report failure' do
        expect(ran_successfully?).to be false
      end

      it 'should say what went wrong' do
        expect(stdout_err).to eq error_message
      end

      context 'with an output file' do
        let(:output_file) { 'spec/files/output.psv' }

        it 'should say what went wrong on stdout' do
          expect(stdout_err).to eq error_message
        end

        it 'should not write a file' do
          expect { stdout_err }.not_to change { File.exist?(output_file) }.from(false)
        end
      end
    end

    context 'with an invalid FINISH_DATE' do
      let(:input_file) { 'spec/files/invalid_finish_month_low.psv' }
      let(:error_message) do
        <<~STR.strip
          Validation error in spec/files/invalid_finish_month_low.psv:
          FINISH_DATE is invalid (must be a date in YYYY-MM-DD format) on line 5
        STR
      end

      it 'should report failure' do
        expect(ran_successfully?).to be false
      end

      it 'should say what went wrong' do
        expect(stdout_err).to eq error_message
      end

      context 'with an output file' do
        let(:output_file) { 'spec/files/output.psv' }

        it 'should say what went wrong on stdout' do
          expect(stdout_err).to eq error_message
        end

        it 'should not write a file' do
          expect { stdout_err }.not_to change { File.exist?(output_file) }.from(false)
        end
      end
    end

    context 'with an invalid CREATED_DATE' do
      let(:input_file) { 'spec/files/invalid_created_hour_high.psv' }

      it 'should report failure' do
        expect(ran_successfully?).to be false
      end

      it 'should say what went wrong' do
        expect(stdout_err).to eq <<~STR.strip
          Validation error in spec/files/invalid_created_hour_high.psv:
          CREATED_DATE is invalid (must be a time stamp in YYYY-MM-DD hh:mm format) on line 5
        STR
      end
    end

    context 'with an invalid CREATED_DATE' do
      let(:input_file) { 'spec/files/invalid_created_hour_bad.psv' }
      let(:error_message) do
        <<~STR.strip
          Validation error in spec/files/invalid_created_hour_bad.psv:
          CREATED_DATE is invalid (must be a time stamp in YYYY-MM-DD hh:mm format) on line 5
        STR
      end

      it 'should report failure' do
        expect(ran_successfully?).to be false
      end

      it 'should say what went wrong' do
        expect(stdout_err).to eq error_message
      end

      context 'with an output file' do
        let(:output_file) { 'spec/files/output.psv' }

        it 'should say what went wrong on stdout' do
          expect(stdout_err).to eq error_message
        end

        it 'should not write a file' do
          expect { stdout_err }.not_to change { File.exist?(output_file) }.from(false)
        end
      end
    end

    context 'with an invalid CREATED_DATE' do
      let(:input_file) { 'spec/files/invalid_created_minute_high.psv' }
      let(:error_message) do
        <<~STR.strip
          Validation error in spec/files/invalid_created_minute_high.psv:
          CREATED_DATE is invalid (must be a time stamp in YYYY-MM-DD hh:mm format) on line 5
        STR
      end

      it 'should report failure' do
        expect(ran_successfully?).to be false
      end

      it 'should say what went wrong' do
        expect(stdout_err).to eq error_message
      end

      context 'with an output file' do
        let(:output_file) { 'spec/files/output.psv' }

        it 'should say what went wrong on stdout' do
          expect(stdout_err).to eq error_message
        end

        it 'should not write a file' do
          expect { stdout_err }.not_to change { File.exist?(output_file) }.from(false)
        end
      end
    end

    context 'with an invalid CREATED_DATE' do
      let(:input_file) { 'spec/files/invalid_created_minute_bad.psv' }
      let(:error_message) do
        <<~STR.strip
          Validation error in spec/files/invalid_created_minute_bad.psv:
          CREATED_DATE is invalid (must be a time stamp in YYYY-MM-DD hh:mm format) on line 5
        STR
      end

      it 'should report failure' do
        expect(ran_successfully?).to be false
      end

      it 'should say what went wrong' do
        expect(stdout_err).to eq error_message
      end
    end
  end

  context 'with a good input file' do
    let(:input_file) { 'spec/files/good_input.psv' }

    it 'should report success' do
      expect(ran_successfully?).to be true
    end

    it 'should output a PSV' do
      expect(stdout_err).to eq <<~PSV.strip
        PROJECT|SHOT|VERSION|STATUS|FINISH_DATE|INTERNAL_BID|CREATED_DATE
        the hobbit|01|64|scheduled|2010-05-15|45.00|2010-04-01 13:35
        lotr|03|16|finished|2001-05-15|15.00|2001-04-01 06:47
        king kong|42|128|not required|2006-07-22|30.00|2006-10-15 09:14
        the hobbit|40|32|finished|2010-05-15|22.80|2010-03-22 01:10
      PSV
    end

    it 'should not contain the duplicated line' do
      expect(stdout_err).not_to match(/king kong\|42\|128\|scheduled\|2006-07-22\|45.00\|2006-08-04 07:22/)
    end

    context 'with an output file' do
      let(:output_file) { 'spec/files/output.psv' }

      it 'should write the PSV to a file' do
        expect do
          expect(stdout_err).to eq "Results written to #{output_file}"
        end.to change { File.exist?(output_file) }.from(false).to(true)

        expect(file_output).to eq <<~PSV.strip
          PROJECT|SHOT|VERSION|STATUS|FINISH_DATE|INTERNAL_BID|CREATED_DATE
          the hobbit|01|64|scheduled|2010-05-15|45.00|2010-04-01 13:35
          lotr|03|16|finished|2001-05-15|15.00|2001-04-01 06:47
          king kong|42|128|not required|2006-07-22|30.00|2006-10-15 09:14
          the hobbit|40|32|finished|2010-05-15|22.80|2010-03-22 01:10
        PSV
      end
    end
  end
end
