describe './bin/query' do
  let(:args) { "#{options} spec/files/shot_versions.psv" }
  let(:cmd) { "./bin/query #{args}" }

  subject(:ran_successfully?) { system("#{cmd} >/dev/null 2>&1") }
  subject(:stdout_err) { `#{cmd}`.strip }

  context 'with -h' do
    let(:args) { '-h' }

    it 'should print usage instructions' do
      expect(stdout_err).to match %r{^Usage: \./bin/query OPTIONS \[INPUT_FILE=shot_versions.psv\]$}
    end
  end

  context 'with a filter' do
    context 'with = operator' do
      context 'and string field' do
        let(:options) { '-f STATUS=scheduled' }

        it 'should report success' do
          expect(ran_successfully?).to be true
        end

        it 'should return the fields where the values match exactly' do
          expect(stdout_err).to eq <<~STR.strip
            PROJECT|SHOT|VERSION|STATUS|FINISH_DATE|INTERNAL_BID|CREATED_DATE
            the hobbit|01|64|scheduled|2010-05-15|45.00|2010-04-01 13:35
          STR
        end
      end

      context 'and integer field' do
        let(:options) { '-f SHOT=01' }

        it 'should report success' do
          expect(ran_successfully?).to be true
        end

        it 'should return the fields where the values match exactly' do
          expect(stdout_err).to eq <<~STR.strip
            PROJECT|SHOT|VERSION|STATUS|FINISH_DATE|INTERNAL_BID|CREATED_DATE
            the hobbit|01|64|scheduled|2010-05-15|45.00|2010-04-01 13:35
          STR
        end
      end

      context 'and float field' do
        let(:options) { '-f INTERNAL_BID=45' }

        it 'should report success' do
          expect(ran_successfully?).to be true
        end

        it 'should return the fields where the values match exactly' do
          expect(stdout_err).to eq <<~STR.strip
            PROJECT|SHOT|VERSION|STATUS|FINISH_DATE|INTERNAL_BID|CREATED_DATE
            the hobbit|01|64|scheduled|2010-05-15|45.00|2010-04-01 13:35
          STR
        end
      end

      context 'and date field' do
        let(:options) { '-f FINISH_DATE=2010-05-15' }

        it 'should report success' do
          expect(ran_successfully?).to be true
        end

        it 'should return the fields where the values match exactly' do
          expect(stdout_err).to eq <<~STR.strip
            PROJECT|SHOT|VERSION|STATUS|FINISH_DATE|INTERNAL_BID|CREATED_DATE
            the hobbit|01|64|scheduled|2010-05-15|45.00|2010-04-01 13:35
            the hobbit|40|32|finished|2010-05-15|22.80|2010-03-22 01:10
          STR
        end
      end
    end

    context 'with \\< operator' do
      context 'and string field' do
        let(:options) { '-f STATUS\\<scheduled' }

        it 'should report success' do
          expect(ran_successfully?).to be true
        end

        it 'should return the fields where the values are less' do
          expect(stdout_err).to eq <<~STR.strip
            PROJECT|SHOT|VERSION|STATUS|FINISH_DATE|INTERNAL_BID|CREATED_DATE
            lotr|03|16|finished|2001-05-15|15.00|2001-04-01 06:47
            king kong|42|128|not required|2006-07-22|30.00|2006-10-15 09:14
            the hobbit|40|32|finished|2010-05-15|22.80|2010-03-22 01:10
          STR
        end
      end

      context 'and integer field' do
        let(:options) { '-f SHOT\\<40' }

        it 'should report success' do
          expect(ran_successfully?).to be true
        end

        it 'should return the fields where the values are less' do
          expect(stdout_err).to eq <<~STR.strip
            PROJECT|SHOT|VERSION|STATUS|FINISH_DATE|INTERNAL_BID|CREATED_DATE
            the hobbit|01|64|scheduled|2010-05-15|45.00|2010-04-01 13:35
            lotr|03|16|finished|2001-05-15|15.00|2001-04-01 06:47
          STR
        end
      end

      context 'and float field' do
        let(:options) { '-f INTERNAL_BID\\<25' }

        it 'should report success' do
          expect(ran_successfully?).to be true
        end

        it 'should return the fields where the values are less' do
          expect(stdout_err).to eq <<~STR.strip
            PROJECT|SHOT|VERSION|STATUS|FINISH_DATE|INTERNAL_BID|CREATED_DATE
            lotr|03|16|finished|2001-05-15|15.00|2001-04-01 06:47
            the hobbit|40|32|finished|2010-05-15|22.80|2010-03-22 01:10
          STR
        end
      end

      context 'and date field' do
        let(:options) { '-f FINISH_DATE\\<2010-05-15' }

        it 'should report success' do
          expect(ran_successfully?).to be true
        end

        it 'should return the fields where the values are less' do
          expect(stdout_err).to eq <<~STR.strip
            PROJECT|SHOT|VERSION|STATUS|FINISH_DATE|INTERNAL_BID|CREATED_DATE
            lotr|03|16|finished|2001-05-15|15.00|2001-04-01 06:47
            king kong|42|128|not required|2006-07-22|30.00|2006-10-15 09:14
          STR
        end
      end
    end

    context 'with \\> operator' do
      context 'and string field' do
        let(:options) { '-f STATUS\\>scheduled' }

        it 'should report success' do
          expect(ran_successfully?).to be true
        end

        it 'should return the fields where the values are greater' do
          expect(stdout_err).to eq <<~STR.strip
            PROJECT|SHOT|VERSION|STATUS|FINISH_DATE|INTERNAL_BID|CREATED_DATE
          STR
        end
      end

      context 'and integer field' do
        let(:options) { '-f SHOT\\>40' }

        it 'should report success' do
          expect(ran_successfully?).to be true
        end

        it 'should return the fields where the values are greater' do
          expect(stdout_err).to eq <<~STR.strip
            PROJECT|SHOT|VERSION|STATUS|FINISH_DATE|INTERNAL_BID|CREATED_DATE
            king kong|42|128|not required|2006-07-22|30.00|2006-10-15 09:14
          STR
        end
      end

      context 'and float field' do
        let(:options) { '-f INTERNAL_BID\\>25' }

        it 'should report success' do
          expect(ran_successfully?).to be true
        end

        it 'should return the fields where the values are greater' do
          expect(stdout_err).to eq <<~STR.strip
            PROJECT|SHOT|VERSION|STATUS|FINISH_DATE|INTERNAL_BID|CREATED_DATE
            the hobbit|01|64|scheduled|2010-05-15|45.00|2010-04-01 13:35
            king kong|42|128|not required|2006-07-22|30.00|2006-10-15 09:14
          STR
        end
      end

      context 'and date field' do
        let(:options) { '-f FINISH_DATE\\>2010-05-15' }

        it 'should report success' do
          expect(ran_successfully?).to be true
        end

        it 'should return the fields where the values are greater' do
          expect(stdout_err).to eq <<~STR.strip
            PROJECT|SHOT|VERSION|STATUS|FINISH_DATE|INTERNAL_BID|CREATED_DATE
          STR
        end
      end
    end

    context 'with \\<= operator' do
      context 'and string field' do
        let(:options) { '-f STATUS\\<=scheduled' }

        it 'should report success' do
          expect(ran_successfully?).to be true
        end

        it 'should return the fields where the values are less or equal' do
          expect(stdout_err).to eq <<~STR.strip
            PROJECT|SHOT|VERSION|STATUS|FINISH_DATE|INTERNAL_BID|CREATED_DATE
            the hobbit|01|64|scheduled|2010-05-15|45.00|2010-04-01 13:35
            lotr|03|16|finished|2001-05-15|15.00|2001-04-01 06:47
            king kong|42|128|not required|2006-07-22|30.00|2006-10-15 09:14
            the hobbit|40|32|finished|2010-05-15|22.80|2010-03-22 01:10
          STR
        end
      end

      context 'and integer field' do
        let(:options) { '-f SHOT\\<=40' }

        it 'should report success' do
          expect(ran_successfully?).to be true
        end

        it 'should return the fields where the values are less or equal' do
          expect(stdout_err).to eq <<~STR.strip
            PROJECT|SHOT|VERSION|STATUS|FINISH_DATE|INTERNAL_BID|CREATED_DATE
            the hobbit|01|64|scheduled|2010-05-15|45.00|2010-04-01 13:35
            lotr|03|16|finished|2001-05-15|15.00|2001-04-01 06:47
            the hobbit|40|32|finished|2010-05-15|22.80|2010-03-22 01:10
          STR
        end
      end

      context 'and float field' do
        let(:options) { '-f INTERNAL_BID\\<=25' }

        it 'should report success' do
          expect(ran_successfully?).to be true
        end

        it 'should return the fields where the values are less or equal' do
          expect(stdout_err).to eq <<~STR.strip
            PROJECT|SHOT|VERSION|STATUS|FINISH_DATE|INTERNAL_BID|CREATED_DATE
            lotr|03|16|finished|2001-05-15|15.00|2001-04-01 06:47
            the hobbit|40|32|finished|2010-05-15|22.80|2010-03-22 01:10
          STR
        end
      end

      context 'and date field' do
        let(:options) { '-f FINISH_DATE\\<=2010-05-15' }

        it 'should report success' do
          expect(ran_successfully?).to be true
        end

        it 'should return the fields where the values are less or equal' do
          expect(stdout_err).to eq <<~STR.strip
            PROJECT|SHOT|VERSION|STATUS|FINISH_DATE|INTERNAL_BID|CREATED_DATE
            the hobbit|01|64|scheduled|2010-05-15|45.00|2010-04-01 13:35
            lotr|03|16|finished|2001-05-15|15.00|2001-04-01 06:47
            king kong|42|128|not required|2006-07-22|30.00|2006-10-15 09:14
            the hobbit|40|32|finished|2010-05-15|22.80|2010-03-22 01:10
          STR
        end
      end
    end

    context 'with \\>= operator' do
      context 'and string field' do
        let(:options) { '-f STATUS\\>=scheduled' }

        it 'should report success' do
          expect(ran_successfully?).to be true
        end

        it 'should return the fields where the values are greater or equal' do
          expect(stdout_err).to eq <<~STR.strip
            PROJECT|SHOT|VERSION|STATUS|FINISH_DATE|INTERNAL_BID|CREATED_DATE
            the hobbit|01|64|scheduled|2010-05-15|45.00|2010-04-01 13:35
          STR
        end
      end

      context 'and integer field' do
        let(:options) { '-f SHOT\\>=40' }

        it 'should report success' do
          expect(ran_successfully?).to be true
        end

        it 'should return the fields where the values are greater or equal' do
          expect(stdout_err).to eq <<~STR.strip
            PROJECT|SHOT|VERSION|STATUS|FINISH_DATE|INTERNAL_BID|CREATED_DATE
            king kong|42|128|not required|2006-07-22|30.00|2006-10-15 09:14
            the hobbit|40|32|finished|2010-05-15|22.80|2010-03-22 01:10
          STR
        end
      end

      context 'and float field' do
        let(:options) { '-f INTERNAL_BID\\>=25' }

        it 'should report success' do
          expect(ran_successfully?).to be true
        end

        it 'should return the fields where the values are greater or equal' do
          expect(stdout_err).to eq <<~STR.strip
            PROJECT|SHOT|VERSION|STATUS|FINISH_DATE|INTERNAL_BID|CREATED_DATE
            the hobbit|01|64|scheduled|2010-05-15|45.00|2010-04-01 13:35
            king kong|42|128|not required|2006-07-22|30.00|2006-10-15 09:14
          STR
        end
      end

      context 'and date field' do
        let(:options) { '-f FINISH_DATE\\>=2010-05-15' }

        it 'should report success' do
          expect(ran_successfully?).to be true
        end

        it 'should return the fields where the values are greater or equal' do
          expect(stdout_err).to eq <<~STR.strip
            PROJECT|SHOT|VERSION|STATUS|FINISH_DATE|INTERNAL_BID|CREATED_DATE
            the hobbit|01|64|scheduled|2010-05-15|45.00|2010-04-01 13:35
            the hobbit|40|32|finished|2010-05-15|22.80|2010-03-22 01:10
          STR
        end
      end
    end

    context 'with != operator' do
      context 'and string field' do
        let(:options) { '-f STATUS!=scheduled' }

        it 'should report success' do
          expect(ran_successfully?).to be true
        end

        it 'should return the fields where the values do not match' do
          expect(stdout_err).to eq <<~STR.strip
            PROJECT|SHOT|VERSION|STATUS|FINISH_DATE|INTERNAL_BID|CREATED_DATE
            lotr|03|16|finished|2001-05-15|15.00|2001-04-01 06:47
            king kong|42|128|not required|2006-07-22|30.00|2006-10-15 09:14
            the hobbit|40|32|finished|2010-05-15|22.80|2010-03-22 01:10
          STR
        end
      end

      context 'and integer field' do
        let(:options) { '-f SHOT!=01' }

        it 'should report success' do
          expect(ran_successfully?).to be true
        end

        it 'should return the fields where the values do not match' do
          expect(stdout_err).to eq <<~STR.strip
            PROJECT|SHOT|VERSION|STATUS|FINISH_DATE|INTERNAL_BID|CREATED_DATE
            lotr|03|16|finished|2001-05-15|15.00|2001-04-01 06:47
            king kong|42|128|not required|2006-07-22|30.00|2006-10-15 09:14
            the hobbit|40|32|finished|2010-05-15|22.80|2010-03-22 01:10
          STR
        end
      end

      context 'and float field' do
        let(:options) { '-f INTERNAL_BID!=45' }

        it 'should report success' do
          expect(ran_successfully?).to be true
        end

        it 'should return the fields where the values do not match' do
          expect(stdout_err).to eq <<~STR.strip
            PROJECT|SHOT|VERSION|STATUS|FINISH_DATE|INTERNAL_BID|CREATED_DATE
            lotr|03|16|finished|2001-05-15|15.00|2001-04-01 06:47
            king kong|42|128|not required|2006-07-22|30.00|2006-10-15 09:14
            the hobbit|40|32|finished|2010-05-15|22.80|2010-03-22 01:10
          STR
        end
      end

      context 'and date field' do
        let(:options) { '-f FINISH_DATE!=2010-05-15' }

        it 'should report success' do
          expect(ran_successfully?).to be true
        end

        it 'should return the fields where the values do not match' do
          expect(stdout_err).to eq <<~STR.strip
            PROJECT|SHOT|VERSION|STATUS|FINISH_DATE|INTERNAL_BID|CREATED_DATE
            lotr|03|16|finished|2001-05-15|15.00|2001-04-01 06:47
            king kong|42|128|not required|2006-07-22|30.00|2006-10-15 09:14
          STR
        end
      end
    end
  end

  context 'with multiple filters' do
    context 'joined by AND' do
      let(:options) { '-f "FINISH_DATE=2010-05-15 AND SHOT>=40"' }

      it 'should report success' do
        expect(ran_successfully?).to be true
      end

      it 'should return the fields where all the filters match' do
        expect(stdout_err).to eq <<~STR.strip
          PROJECT|SHOT|VERSION|STATUS|FINISH_DATE|INTERNAL_BID|CREATED_DATE
          the hobbit|40|32|finished|2010-05-15|22.80|2010-03-22 01:10
        STR
      end
    end

    context 'joined by OR' do
      let(:options) { '-f "FINISH_DATE=2010-05-15 OR SHOT>=40"' }

      it 'should report success' do
        expect(ran_successfully?).to be true
      end

      it 'should return the fields where all the filters match' do
        expect(stdout_err).to eq <<~STR.strip
          PROJECT|SHOT|VERSION|STATUS|FINISH_DATE|INTERNAL_BID|CREATED_DATE
          the hobbit|01|64|scheduled|2010-05-15|45.00|2010-04-01 13:35
          king kong|42|128|not required|2006-07-22|30.00|2006-10-15 09:14
          the hobbit|40|32|finished|2010-05-15|22.80|2010-03-22 01:10
        STR
      end
    end

    context 'both AND and OR' do
      let(:options) { "-f 'PROJECT=\"the hobbit\" AND SHOT=1 OR SHOT<40'" }

      it 'should report success' do
        expect(ran_successfully?).to be true
      end

      it 'should return the fields where all the filters match' do
        expect(stdout_err).to eq <<~STR.strip
          PROJECT|SHOT|VERSION|STATUS|FINISH_DATE|INTERNAL_BID|CREATED_DATE
          the hobbit|01|64|scheduled|2010-05-15|45.00|2010-04-01 13:35
          lotr|03|16|finished|2001-05-15|15.00|2001-04-01 06:47
        STR
      end
    end

    context 'both AND and bracketed OR' do
      let(:options) { "-f 'PROJECT=\"the hobbit\" AND (SHOT=1 OR SHOT<40)'" }

      it 'should report success' do
        expect(ran_successfully?).to be true
      end

      it 'should return the fields where all the filters match' do
        expect(stdout_err).to eq <<~STR.strip
          PROJECT|SHOT|VERSION|STATUS|FINISH_DATE|INTERNAL_BID|CREATED_DATE
          the hobbit|01|64|scheduled|2010-05-15|45.00|2010-04-01 13:35
        STR
      end
    end

    context 'Bobby Tables' do # see https://xkcd.com/327/
      let(:options) { "-f 'FileUtils.rm(\"spec/files/output.psv\") #'" }

      it 'should report failure' do
        expect(ran_successfully?).to be false
      end

      it 'should say what went wrong' do
        expect(stdout_err).to eq 'Syntax error in FileUtils.rm("spec/files/output.psv") #'
      end

      it 'should leave the file intact' do
        expect { stdout_err }.not_to change { File.exist?('spec/files/output.psv') }.from(true)
      end
    end
  end

  context 'with an order' do
    context 'on a string field' do
      let(:options) { '-o PROJECT' }

      it 'should report success' do
        expect(ran_successfully?).to be true
      end

      it 'should return the records in the given order' do
        expect(stdout_err).to eq <<~STR.strip
          PROJECT|SHOT|VERSION|STATUS|FINISH_DATE|INTERNAL_BID|CREATED_DATE
          king kong|42|128|not required|2006-07-22|30.00|2006-10-15 09:14
          lotr|03|16|finished|2001-05-15|15.00|2001-04-01 06:47
          the hobbit|01|64|scheduled|2010-05-15|45.00|2010-04-01 13:35
          the hobbit|40|32|finished|2010-05-15|22.80|2010-03-22 01:10
        STR
      end
    end

    context 'on an integer field' do
      let(:options) { '-o SHOT' }

      it 'should report success' do
        expect(ran_successfully?).to be true
      end

      it 'should return the records in the given order' do
        expect(stdout_err).to eq <<~STR.strip
          PROJECT|SHOT|VERSION|STATUS|FINISH_DATE|INTERNAL_BID|CREATED_DATE
          the hobbit|01|64|scheduled|2010-05-15|45.00|2010-04-01 13:35
          lotr|03|16|finished|2001-05-15|15.00|2001-04-01 06:47
          the hobbit|40|32|finished|2010-05-15|22.80|2010-03-22 01:10
          king kong|42|128|not required|2006-07-22|30.00|2006-10-15 09:14
        STR
      end
    end

    context 'on a float field' do
      let(:options) { '-o INTERNAL_BID' }

      it 'should report success' do
        expect(ran_successfully?).to be true
      end

      it 'should return the records in the given order' do
        expect(stdout_err).to eq <<~STR.strip
          PROJECT|SHOT|VERSION|STATUS|FINISH_DATE|INTERNAL_BID|CREATED_DATE
          lotr|03|16|finished|2001-05-15|15.00|2001-04-01 06:47
          the hobbit|40|32|finished|2010-05-15|22.80|2010-03-22 01:10
          king kong|42|128|not required|2006-07-22|30.00|2006-10-15 09:14
          the hobbit|01|64|scheduled|2010-05-15|45.00|2010-04-01 13:35
        STR
      end
    end

    context 'on a date field' do
      let(:options) { '-o FINISH_DATE' }

      it 'should report success' do
        expect(ran_successfully?).to be true
      end

      it 'should return the records in the given order' do
        expect(stdout_err).to eq <<~STR.strip
          PROJECT|SHOT|VERSION|STATUS|FINISH_DATE|INTERNAL_BID|CREATED_DATE
          lotr|03|16|finished|2001-05-15|15.00|2001-04-01 06:47
          king kong|42|128|not required|2006-07-22|30.00|2006-10-15 09:14
          the hobbit|01|64|scheduled|2010-05-15|45.00|2010-04-01 13:35
          the hobbit|40|32|finished|2010-05-15|22.80|2010-03-22 01:10
        STR
      end
    end

    context 'on multiple fields' do
      let(:options) { '-o FINISH_DATE,SHOT' }

      it 'should report success' do
        expect(ran_successfully?).to be true
      end

      it 'should return the records in the given order' do
        expect(stdout_err).to eq <<~STR.strip
          PROJECT|SHOT|VERSION|STATUS|FINISH_DATE|INTERNAL_BID|CREATED_DATE
          lotr|03|16|finished|2001-05-15|15.00|2001-04-01 06:47
          king kong|42|128|not required|2006-07-22|30.00|2006-10-15 09:14
          the hobbit|01|64|scheduled|2010-05-15|45.00|2010-04-01 13:35
          the hobbit|40|32|finished|2010-05-15|22.80|2010-03-22 01:10
        STR
      end
    end
  end

  context 'with a field selected' do
    let(:options) { '-s PROJECT' }

    it 'should report success' do
      expect(ran_successfully?).to be true
    end

    it 'should return the selected field' do
      expect(stdout_err).to eq <<~STR.strip
        PROJECT
        the hobbit
        lotr
        king kong
        the hobbit
      STR
    end
  end

  context 'with multiple fields selected' do
    let(:options) { '-s CREATED_DATE,PROJECT' }

    it 'should report success' do
      expect(ran_successfully?).to be true
    end

    it 'should return the selected fields in the order given' do
      expect(stdout_err).to eq <<~STR.strip
        CREATED_DATE|PROJECT
        2010-04-01 13:35|the hobbit
        2001-04-01 06:47|lotr
        2006-10-15 09:14|king kong
        2010-03-22 01:10|the hobbit
      STR
    end
  end

  context 'with grouping and good aggregates' do
    let(:options) do
      '-g PROJECT -s PROJECT,INTERNAL_BID:sum,SHOT:collect,FINISH_DATE:count,CREATED_DATE:min,STATUS:max'
    end

    it 'should report success' do
      expect(ran_successfully?).to be true
    end

    it 'should return the selected fields aggregated correctly' do
      expect(stdout_err).to eq <<~STR.strip
        PROJECT|INTERNAL_BID|SHOT|FINISH_DATE|CREATED_DATE|STATUS
        the hobbit|67.80|[01,40]|2|2010-03-22 01:10|scheduled
        lotr|15.00|[03]|1|2001-04-01 06:47|finished
        king kong|30.00|[42]|1|2006-10-15 09:14|not required
      STR
    end
  end

  context 'with grouping and bad aggregates' do
    let(:options) { '-g PROJECT -s PROJECT,STATUS:sum' }

    it 'should report failure' do
      expect(ran_successfully?).to be false
    end

    it 'should say what went wrong' do
      expect(stdout_err).to eq "Can't sum the STATUS field"
    end
  end

  context 'with grouping and unaggregated fields' do
    let(:options) { '-g PROJECT -s PROJECT,INTERNAL_BID' }

    it 'should report failure' do
      expect(ran_successfully?).to be false
    end

    it 'should say what went wrong' do
      expect(stdout_err).to eq 'INTERNAL_BID is neither aggregated nor a group'
    end
  end

  context 'with good aggregates but no grouping' do
    let(:options) { '-s INTERNAL_BID:sum' }

    it 'should report success' do
      expect(ran_successfully?).to be true
    end

    it 'should aggregate all records' do
      expect(stdout_err).to eq <<~STR.strip
        INTERNAL_BID
        112.80
      STR
    end
  end

  context 'with aggregates and unaggregated fields but no grouping' do
    let(:options) { '-s PROJECT,INTERNAL_BID:sum' }

    it 'should report failure' do
      expect(ran_successfully?).to be false
    end

    it 'should say what went wrong' do
      expect(stdout_err).to eq 'PROJECT is neither aggregated nor a group'
    end
  end

  context 'with bad aggregates but no grouping' do
    let(:options) { '-s PROJECT,STATUS:sum' }

    it 'should report failure' do
      expect(ran_successfully?).to be false
    end

    it 'should say what went wrong' do
      expect(stdout_err).to eq "Can't sum the STATUS field"
    end
  end
end
